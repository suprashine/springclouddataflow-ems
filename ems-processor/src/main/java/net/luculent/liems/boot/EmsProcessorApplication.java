package net.luculent.liems.boot;

import java.util.Map.Entry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.integration.annotation.Transformer;

import com.alibaba.fastjson.JSONObject;

@EnableBinding(Processor.class)
@SpringBootApplication
public class EmsProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmsProcessorApplication.class, args);
	}

	@Transformer(inputChannel = Processor.INPUT, outputChannel = Processor.OUTPUT)
	public String transform(Object msg) {
		System.out.println("接收数据" + msg.toString());
		JSONObject jo = JSONObject.parseObject(msg.toString());
		for (Entry<String, Object> entry : jo.entrySet()) {
			JSONObject temp = (JSONObject)entry.getValue();
			temp.put("p", temp.getFloat("p")+200);
			temp.put("r", temp.getFloat("r")+100);
			temp.put("g", temp.getFloat("g")+60);
        }
		return jo.toJSONString();
	}
}
