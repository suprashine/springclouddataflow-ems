package net.luculent.liems.boot;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

import net.luculent.liems.boot.service.WebSocketServer;

@EnableBinding(value = {Sink.class})
@SpringBootApplication
public class EmsSinkApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmsSinkApplication.class, args);
	}
	
	@StreamListener(Sink.INPUT)//将被修饰的方法注册为消息中间件上数据流的事件监听器，
    public void receive(Object msg) {
		System.out.println("接收数据" + msg.toString());
//		JSONObject jo = JSONObject.parseObject(msg.toString());
		try {
			WebSocketServer.sendInfo(msg.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }	
}
