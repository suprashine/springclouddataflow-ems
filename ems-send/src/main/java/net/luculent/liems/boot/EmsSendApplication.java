package net.luculent.liems.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.ConfigurableApplicationContext;

import net.luculent.liems.boot.service.WebSocketServer;

@SpringBootApplication
@EnableBinding(Source.class)
public class EmsSendApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(EmsSendApplication.class, args);
		WebSocketServer.setApplicationContext(context);
	}
	
//	@Bean
//    @InboundChannelAdapter(value = Source.OUTPUT, poller = @Poller(fixedDelay = "1000", maxMessagesPerPoll = "1"))
//    public MessageSource<Power> timeMessageSource() {
//		Power p = new Power();
//		p.setCurrent(65.4f);
//		p.setActive(0.5f);
//		p.setReactive(-53.4f);
//		try {
//			System.out.println(JSON.toJSONString(p));
//			WebSocketServer.sendInfo(JSON.toJSONString(p));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//        return () -> MessageBuilder.withPayload(p).build();
//    }
}
