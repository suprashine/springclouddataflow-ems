package net.luculent.liems.boot.domin;

public class Power {
	private float current;
	private float active;
	private float reactive;
	public float getCurrent() {
		return current;
	}
	public void setCurrent(float current) {
		this.current = current;
	}
	public float getActive() {
		return active;
	}
	public void setActive(float active) {
		this.active = active;
	}
	public float getReactive() {
		return reactive;
	}
	public void setReactive(float reactive) {
		this.reactive = reactive;
	}
}
