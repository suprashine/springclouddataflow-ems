package net.luculent.liems.boot.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

import net.luculent.liems.boot.domin.Power;

@Service
public class SourceService {

	@Autowired
	private Source source;
	
	public void sendData(){
		Power p = new Power();
		p.setCurrent(65.4f);
		p.setActive(0.5f);
		p.setReactive(-53.4f);
		try {
			WebSocketServer.sendInfo(JSON.toJSONString(p));
		} catch (IOException e) {
			e.printStackTrace();
		}
		source.output().send(MessageBuilder.withPayload(p).build());
	}
}
