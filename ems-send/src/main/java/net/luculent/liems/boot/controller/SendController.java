package net.luculent.liems.boot.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.luculent.liems.boot.service.WebSocketServer;

@RestController
public class SendController {
	
	@Autowired
	private Source source;

	@RequestMapping("sendData")
	public String sendData(@RequestParam(value="a") String a){
		try {
			WebSocketServer.sendInfo(a);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		source.output().send(MessageBuilder.withPayload(a).build());
		return "success";
	}
}
