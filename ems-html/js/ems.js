var vm = new Vue({
  el: '#app',
  data: {send:{a:{p:0,r:0,g:0},b:{p:0,r:0,g:0},c:{p:0,r:0,g:0},d:{p:0,r:0,g:0},e:{p:0,r:0,g:0},f:{p:0,r:0,g:0}},
         recieve:{a:{p:0,r:0,g:0},b:{p:0,r:0,g:0},c:{p:0,r:0,g:0},d:{p:0,r:0,g:0},e:{p:0,r:0,g:0},f:{p:0,r:0,g:0}}},
  methods: {
    refresh: function (event) {
      update();
    },
  	auto_refresh:function(){
  		window.setInterval("update()", 1000);
  	}
  }
});

function update(){
   vm.send.a.p = randomNum(60,70);
   vm.send.a.r = randomNum(0,1);
   vm.send.a.g = randomNum(-60,-20);

   vm.send.b.p = randomNum(60,70);
   vm.send.b.r = randomNum(1,2);
   vm.send.b.g = randomNum(-60,-30);

   vm.send.c.p = randomNum(1400,1450);
   vm.send.c.r = randomNum(-1300,-1200);
   vm.send.c.g = randomNum(-40,-30);

   vm.send.d.p = randomNum(1000,1100);
   vm.send.d.r = randomNum(900,1000);
   vm.send.d.g = randomNum(-15,-10);

   vm.send.e.p = randomNum(1000,1100);
   vm.send.e.r = randomNum(900,950);
   vm.send.e.g = randomNum(-10,-5);

   vm.send.f.p = randomNum(1400,1450);
   vm.send.f.r = randomNum(-1300,-1200);
   vm.send.f.g = randomNum(-15,10);
   
   ws.send(JSON.stringify(vm.send));
}

//生成从minNum到maxNum的随机数
function randomNum(minNum,maxNum){ 
    switch(arguments.length){ 
        case 1: 
            return (Math.random()*minNum+1).toFixed(1);break; 
        case 2: 
            return (Math.random()*(maxNum-minNum+1)+minNum).toFixed(1);break; 
		default: 
			return 0;break; 
    } 
}

var ws = new WebSocket("ws://localhost:8001/websocket");
var ws2 = new WebSocket("ws://localhost:8003/websocket");
ws2.onmessage = function (evt) { 
  var received_msg = evt.data;
  vm.recieve = JSON.parse(received_msg);
};

function WebSocketTest(){
	if ("WebSocket" in window)
	{
	   //alert("您的浏览器支持 WebSocket!");
	   
	   // 打开一个 web socket
	   var ws = new WebSocket("ws://localhost:8080/websocket");
		
	   ws.onopen = function()
	   {
		  // Web Socket 已连接上，使用 send() 方法发送数据
		  ws.send("发送数据");
		  //alert("数据发送中...");
	   };
		
	   ws.onmessage = function (evt) 
	   { 
		  var received_msg = evt.data;
		  document.getElementById("msg").value = received_msg;
		  //alert("数据已接收...");
	   };
		
	   /*ws.onclose = function()
	   { 
		  // 关闭 websocket
		  alert("连接已关闭..."); 
	   };*/
	}
	else
	{
	   // 浏览器不支持 WebSocket
	   alert("您的浏览器不支持 WebSocket!");
	}
}